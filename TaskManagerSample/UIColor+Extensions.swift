//
//  UIColor+Extensions.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/14/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import UIKit

// For custom colors that we can keep a track of
extension UIColor {
  
  class var themeNavigationBar: UIColor {
    get {
      return UIColor(colorLiteralRed: 30/255, green: 70/255, blue: 135/255, alpha: 1)
    }
  }
  
  class var themeTableViewBackground: UIColor {
    get {
      return UIColor(colorLiteralRed: 200/255, green: 215/255, blue: 230/255, alpha: 1)
    }
  }
  
  class var titleBarComplete: UIColor {
    get {
      return UIColor(colorLiteralRed: 110/255, green: 215/255, blue: 140/255, alpha: 1)
    }
  }
  
  class var titleBarIncomplete: UIColor {
    get {
      return UIColor(colorLiteralRed: 240/255, green: 240/255, blue: 240/255, alpha: 1)
    }
  }
}
