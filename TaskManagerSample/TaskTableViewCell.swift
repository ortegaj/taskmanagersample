//
//  TaskTableViewCell.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/14/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
  
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var expandButton: UIButton!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var bodyView: UIView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var usernameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var checkButton: UIButton!
  
  // Attach a didSet property observer to automatically configure the cell upon setting the model
  var todoPresenter: TodoPresenter! {
    didSet {
      configure()
    }
  }
  
  /// An action that is performed once the user taps the expand button. Set by the view model to update the table view.
  var expandClosure: (() -> Void)?
  
  /// Configures the cell based on the model
  private func configure() {
    
    titleLabel.text = todoPresenter.todo.title
    
    nameLabel.text = "Name: \(todoPresenter.todo.user.name)"
    usernameLabel.text = "UserName: \(todoPresenter.todo.user.userName)"
    emailLabel.text = "Email: \(todoPresenter.todo.user.email)"
    
    if todoPresenter.isExpanded {
      expandButton.setImage(UIImage(named: "iconChevronUp"), for: .normal)
    }
    else {
      expandButton.setImage(UIImage(named: "iconChevronDown"), for: .normal)
    }
    
    if todoPresenter.todo.completed {
      topView.backgroundColor = UIColor.titleBarComplete
      checkButton.tintColor = UIColor.titleBarComplete
      checkButton.setImage(UIImage(named: "iconCheckBoxFilled"), for: .normal)
    }
    else {
      topView.backgroundColor = UIColor.titleBarIncomplete
      checkButton.tintColor = UIColor.black
      checkButton.setImage(UIImage(named: "iconCheckBoxEmpty"), for: .normal)
    }
  }
  
  @IBAction func tappedExpandButton(_ sender: Any) {
    let expanded = !todoPresenter.isExpanded
    todoPresenter.isExpanded = expanded
    let imageString = expanded ? "iconChevronUp" : "iconChevronDown"
    expandButton.setImage(UIImage(named: imageString), for: .normal)
    
    if !expanded {
      UIView.animate(withDuration: 0.3, animations: {
        self.bodyView.frame.size.height = 0
      })
    }
    
    expandClosure?()
  }
  
  @IBAction func tappedCheckButton(_ sender: Any) {
    let completed = !todoPresenter.todo.completed
    todoPresenter.todo.completed = completed
    
    let newTitleColor = completed ? UIColor.titleBarComplete : UIColor.titleBarIncomplete
    let newCheckBoxTintColor = completed ? UIColor.titleBarComplete : UIColor.black
    let imageString = completed ? "iconCheckBoxFilled" : "iconCheckBoxEmpty"
    
    checkButton.setImage(UIImage(named: imageString), for: .normal)
    
    UIView.animate(withDuration: 0.2, animations: {
      self.topView.backgroundColor = newTitleColor
      self.checkButton.tintColor = newCheckBoxTintColor
    })
  }
  
}
