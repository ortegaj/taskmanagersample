//
//  TaskManagerViewModel.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/13/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

/// Provides an interface to work with data for the TodoViewController
protocol TaskManagerViewModel {
  
  var delegate: TaskManagerViewModelDelegate? { get set }
  func retrieveData()
  func numberOfItems() -> Int
  func cellForItem(tableView: UITableView, atRow row: Int) -> UITableViewCell
  func heightForCell(atRow row: Int) -> CGFloat
}

/// Required for communication to the view controller this view model belongs to.
protocol TaskManagerViewModelDelegate: class {
  
  func dataRetrievalSucceeded()
  func dataRetrievalFailed()
}

/// The default TodoViewModel object. Gets data from https://jsonplaceholder.typicode.com/
class SampleTaskManagerViewModel: TaskManagerViewModel {
  
  weak var delegate: TaskManagerViewModelDelegate?
  var todos: [TodoPresenter] = []
  
  /// Retrieves user and todo data, then builds the todos array
  func retrieveData() {
    
    let urlRoot = "https://jsonplaceholder.typicode.com"
    let group = DispatchGroup()
    var userJSON: JSON?
    var todoJSON: JSON?
    
    group.enter()
    Alamofire.request("\(urlRoot)/users").responseJSON { response in
      
      switch(response.result) {
      case .success(let json):
        userJSON = JSON(json)
      case .failure(let error):
        print("There was an error getting user data: \(error.localizedDescription)")
      }
      
      group.leave()
    }
    
    group.enter()
    Alamofire.request("\(urlRoot)/todos").responseJSON { response in
      
      switch(response.result) {
      case .success(let json):
        todoJSON = JSON(json)
      case .failure(let error):
        print("There was an error getting todo data: \(error.localizedDescription)")
      }
      
      group.leave()
    }
    
    group.notify(queue: DispatchQueue.main, execute: {
      
      guard let user = userJSON, let todo = todoJSON else {
        self.delegate?.dataRetrievalFailed()
        return
      }
      
      // Build TodoPresenter objects with our retrieved data.
      let userDictionary = self.buildUserDictionary(usingJSON: user)
      self.todos = self.buildTodoPresenterArray(usingJSON: todo, withUserDictionary: userDictionary)
      self.delegate?.dataRetrievalSucceeded()
    })
  }
  
  /// Builds and returns a User object dictionary using a JSON object
  private func buildUserDictionary(usingJSON json: JSON) -> [Int : User] {
    
    var userDictionary = [Int : User]()
    
    for (_, j) in json {
      guard let user = User(json: j) else {
        continue
      }
      userDictionary[user.id] = user
    }
    
    return userDictionary
  }
  
  /// Builds and returns a TodoPresenter object array using a JSON object and a User object dictionary.
  /// The dictionary is used to set the user parameter of the Todo object
  private func buildTodoPresenterArray(usingJSON json: JSON, withUserDictionary userDictionary: [Int : User]) -> [TodoPresenter] {
    
    var todoArray = [TodoPresenter]()
    
    for (_, j) in json {
      
      // The instantiation of the Todo object is considered a failure if we cannot get the required
      // properties from the json object and we cannot get a user object from the dictionary.
      // Do not instantiate a TodoPresenter if we cannot build the Todo object.
      guard let userId = j["userId"].int,
        let user = userDictionary[userId],
        let todo = Todo(json: j, user: user) else {
        continue
      }
      todoArray.append(TodoPresenter(todo: todo))
    }
    
    return todoArray
  }
  
  /// Returns number of cells to show
  func numberOfItems() -> Int {
    return todos.count
  }
  
  /// Builds and returns a cell for the specified row
  func cellForItem(tableView: UITableView, atRow row: Int) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "TaskTableViewCell") as! TaskTableViewCell
    cell.todoPresenter = todos[row]
    cell.expandClosure = {
      tableView.beginUpdates()
      tableView.endUpdates()
    }
    return cell
  }
  
  /// Returns the correct height for cells
  func heightForCell(atRow row: Int) -> CGFloat {
    return todos[row].isExpanded ? 240 : 62
  }
}
