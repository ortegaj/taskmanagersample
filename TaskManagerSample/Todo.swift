//
//  Todo.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/13/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import SwiftyJSON

/// The Todo class represents a todo object obtained from https://jsonplaceholder.typicode.com/todos
class Todo {
  
  let id: Int
  let title: String
  var completed: Bool
  var user: User
  
  /// Initializes a Todo object using JSON and a User object. Fails and returns nil if required data is missing.
  init?(json: JSON, user: User) {
    guard
      let id = json["id"].int,
      let title = json["title"].string,
      let completed = json["completed"].bool else {
        return nil
    }
    
    self.id = id
    self.title = title
    self.completed = completed
    self.user = user
  }
}
