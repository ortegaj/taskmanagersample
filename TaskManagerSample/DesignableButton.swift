//
//  DesignableButton.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/13/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import UIKit

/// A button with an IBInspectable corner radius. Used to set the corner radius easily from the interface builder
@IBDesignable class DesignableButton: UIButton {
  
  @IBInspectable var cornerRadius: CGFloat = 0 {
    didSet {
      layer.masksToBounds = cornerRadius > 0
      layer.cornerRadius = cornerRadius
    }
  }
}
