//
//  TaskManagerViewController.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/13/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import UIKit

class TaskManagerViewController: UIViewController {
  
  //MARK: IBOutlets
  @IBOutlet weak var tableView: UITableView!
  
  //MARK: Properties
  var viewModel: TaskManagerViewModel = SampleTaskManagerViewModel()
  var loadingView: LoadingView?
  
  //MARK: Overrides
  override func viewDidLoad() {
    super.viewDidLoad()
    setupLoadingView()
    setupViewModel()
  }
  
  //MARK: Methods
  private func setupLoadingView() {
    guard let lv = Bundle.main.loadNibNamed("LoadingViewNib", owner: nil, options: nil)?.first as? LoadingView else {
      fatalError("Failed to initialize LoadingView")
    }
    loadingView = lv
    lv.tryAgainCallback = {
      [weak self] in
      self?.tappedTryAgain()
    }
    
    view.addSubview(lv)
    view.bringSubview(toFront: lv)
    lv.frame = view.bounds
    lv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    lv.alpha = 0
    UIView.animate(withDuration: 0.2, animations: {
      lv.alpha = 1
    })
  }
  
  private func setupViewModel() {
    viewModel.delegate = self
    viewModel.retrieveData()
  }
  
  private func tappedTryAgain() {
    loadingView?.setLoadingState()
    viewModel.retrieveData()
  }
}

//MARK: TaskManagerViewModelDelegate methods
extension TaskManagerViewController: TaskManagerViewModelDelegate {
  
  func dataRetrievalSucceeded() {
    tableView.reloadData()
    UIView.animate(withDuration: 0.2, animations: {
      self.loadingView?.alpha = 0
    }, completion: {
      _ in
      self.loadingView?.removeFromSuperview()
      self.loadingView = nil
    })
  }
  
  func dataRetrievalFailed() {
    loadingView?.setErrorState()
  }
}

//MARK: UITableViewDelegate / DataSource methods
extension TaskManagerViewController: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfItems()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return viewModel.cellForItem(tableView: tableView, atRow: indexPath.row)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return viewModel.heightForCell(atRow: indexPath.row)
  }
}
