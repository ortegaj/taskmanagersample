//
//  LoadingView.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/13/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import UIKit

/// This view is shown when the application is loading resources.
class LoadingView: UIView {
  
  @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet private weak var retryButton: DesignableButton!
  @IBOutlet private weak var label: UILabel!
  
  /// This is set by the view controller that uses this view. The closure triggers when a user taps the Try Again button
  var tryAgainCallback: (() -> Void)?
  
  override func awakeFromNib() {
    retryButton.alpha = 0
    retryButton.isEnabled = false
    label.alpha = 0
  }
  
  /// Alters the views to present to the user that the application is loading
  func setLoadingState() {
    activityIndicator.startAnimating()
    retryButton.isEnabled = false
    UIView.animate(withDuration: 0.2, animations: {
      self.retryButton.alpha = 0
      self.label.alpha = 0
    })
  }
  
  /// Alters the views to present to the user that the application has encountered an error
  func setErrorState() {
    activityIndicator.stopAnimating()
    retryButton.isEnabled = true
    UIView.animate(withDuration: 0.2, animations: {
      self.retryButton.alpha = 1
      self.label.alpha = 1
    })
  }
  
  @IBAction func tappedTryAgainButton(_ sender: Any) {
    tryAgainCallback?()
  }
  
}
