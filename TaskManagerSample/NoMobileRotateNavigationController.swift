//
//  NoMobileRotateNavigationController.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/13/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import UIKit

/// A UINavigationController that does not allow for rotation on mobile
class NoMobileRotateNavigationController: UINavigationController {
  
  // Do not auto rotate if we're using a phone
  override var shouldAutorotate: Bool {
    if UIDevice.current.userInterfaceIdiom == .phone {
      return false
    }
    return true
  }
  
  // Do not support landscape orientations if we're using a phone
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    if UIDevice.current.userInterfaceIdiom == .pad {
      return [.portrait, .landscape]
    }
    else {
      return [.portrait]
    }
  }
}
