//
//  User.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/13/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import SwiftyJSON

/// The User class represents a user object obtained from https://jsonplaceholder.typicode.com/users
class User {
  
  let id: Int
  let userName: String
  let email: String
  let name: String
  
  /// Initializes a User object using JSON. Fails and returns nil if required data is missing.
  init?(json: JSON) {
    guard
      let id = json["id"].int,
      let userName = json["username"].string,
      let email = json["email"].string,
      let name = json["name"].string else {
      return nil
    }
    
    self.id = id
    self.userName = userName
    self.email = email
    self.name = name
  }
}
