//
//  TodoPresenter.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/14/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

/// A wrapper class for Todo objects. Meant to separate the model from how it should be presented on the screen.
class TodoPresenter {
  
  let todo: Todo
  var isExpanded = false
  
  init(todo: Todo) {
    self.todo = todo
  }
}
