//
//  ThemeManager.swift
//  TaskManagerSample
//
//  Created by Joshua Ortega on 8/14/17.
//  Copyright © 2017 Ortega. All rights reserved.
//

import UIKit

/// This class sets the colors of certain views using UIAppearance
class ThemeManager {
  
  // Should not be initializable
  private init() {}
  
  // Our default app theme
  class func setDefaultTheme() {
    
    UIApplication.shared.statusBarStyle = .lightContent
    UITableView.appearance().backgroundColor = UIColor.themeTableViewBackground
    UINavigationBar.appearance().barTintColor = UIColor.themeNavigationBar
    UINavigationBar.appearance().titleTextAttributes = [
      NSForegroundColorAttributeName: UIColor.white
    ]
  }
}
